(taken from https://static.gamedevmarket.net/terms-conditions/#pro-licence, @30.04.2021 16:21)

Terms & Conditions
Marketplace Terms

This website is owned and operated by GameDev Network Limited, a company incorporated in England (company number 8951892; VAT number 202253758) whose registered office is at Baltic Co-Working Space, 12 Jordan Street, Liverpool, UK, L1 0BP (“GDN” or “we” or “us”). Our site features an online marketplace which allows registered users of our site (“Members“) to sell and purchase a licence in respect of Assets (“Marketplace”).

These terms (“Marketplace Terms”) govern how the Marketplace operates, how Assets may be licensed via the Marketplace, how such licences may be sold and purchased and how the relevant Assets may be used. These Marketplace Terms apply in addition to the Terms of Website Use, Acceptable Use Policy, Privacy Policy and our Cookie Policy.

Please read these Marketplace Terms carefully and make sure that you understand them, before using the Marketplace. In particular, please note the provisions of clauses 3.6 and 3.7 which affect your cancellation rights.

Please note that by using the Marketplace (whether as a Purchaser or Seller), you agree to be bound by these Marketplace Terms and the other documents expressly referred to in it.

1. DEFINITIONS:

1. In these Marketplace Terms, the following definitions apply (in addition to the other definitions herein):

“Asset(s)” means any work, information, data, software, executable code, image, drawing, animation, audio content or video content in any digital medium or form including (but not limited to) 2D image files, 3D design files, GUI elements and audio files.

“Derivative Work” means a modification or addition to a Licensed Asset or any other form in which a Licensed Asset may be recast, transformed or adapted.

“Licence(s)” has the definition set out in clause 4.1.

“Licensed Asset” means an Asset in respect of which a Licence has been sold by a Seller and purchased by a Purchaser via the Marketplace.

“Media Product” means any digital and/or media product, creation or platform of a Purchaser including (but not limited to) software, applications, video content, audio content, documents and websites.

“Monetized Media Product” means a Media Product which, in addition to any original sale price of the Media Product, is capable of producing further income, profits, gains and any other financial consideration, value, receipt or measure for any party by any means whatsoever, including (but not limited to) via in-app purchase facilities or advertising.

“Non-Monetized Media Product” means a Media Product in relation to which, other than the original sale price of the Media Product, no party is capable of receiving any further income, profits, gains and any other financial consideration, value, receipt or measure by any means whatsoever, including (but not limited to) via in-app purchase facilities or advertising.

“Purchaser” means a Member who purchases a Licence of an Asset by way of sub-licence from GDN via the Marketplace.

“Seller” a Member who offers to grant a Licence of an Asset to GDN for sub-license to other Members via the Marketplace.

2. THE MARKETPLACE

2.1. Members may;

(a) sell Licences in respect of their Assets to GDN for sub-license to other Members via the Marketplace;

(b) purchase Licences by way of sub-licence from GDN in respect of other Members’ Assets via the Marketplace; and

(c) browse or access the Marketplace;

in accordance with these Marketplace Terms.

3. PURCHASE OF LICENCES 

3.1. The Licences dictate how the Assets may be used by the Purchaser.

3.2. GDN is party to Licences purely for the purpose of licensing the applicable Licensed Asset from Sellers and sub-licensing the applicable Licensed Asset to Purchasers.

3.3. The prices of the Licences (“Purchase Prices”) will be as quoted on the Marketplace from time to time.

3.4. The Marketplace contains a large number of Assets. The Purchaser and Seller acknowledge and agree that despite GDN’s reasonable precautions, Purchase Prices may be listed at an incorrect price or with incorrect information due to a typographical error or similar oversight. In these circumstances, GDN reserves the right to cancel or reverse a transaction, even after an order has been confirmed and a payment has been processed. If a transaction is cancelled, GDN will arrange for any payment to be credited or refunded.

3.5. For the steps the Purchaser needs to take to place an order on the Marketplace, see the “How To Buy” page.

3.6. By virtue of submitting an order for a Licence via the Marketplace, the Purchaser shall be deemed to have consented to being supplied the applicable Licensed Asset for download in accordance with clause 3.13 before the end of any statutory cancellation period to which the Purchaser would otherwise be entitled and the Purchaser acknowledges that, as a result, the Purchaser will lose any such cancellation right and GDN hereby confirms to the Purchaser such consent and acknowledgement.

3.7. Neither the Seller nor the Purchaser may cancel an order for a Licence once the order has been submitted via the Marketplace.

3.8. GDN does not give any undertaking as to the continued availability of Assets offered for Licence via the Marketplace.

3.9 The contracts between the Seller and GDN and between the Purchaser and GDN will each only be formed when GDN confirms acceptance of an order.

3.10. The Purchaser must pay the Purchase Price in full via Paypal. 

3.11. Once the Purchase Price has been received by GDN, GDN will send the Purchaser and Seller an e-mail that confirms that the Purchase Price has been received (“Purchase Confirmation”). 

3.12. A Licence shall be granted simultaneously from the Seller to GDN and (by way of sub-licence thereof) from GDN to the Purchaser and shall become effective once GDN has issued the Purchase Confirmation. For the avoidance of doubt, GDN’s entry into a Licence with a Seller is subject to and conditional upon GDN entering into a related Licence with a Purchaser and receiving the Purchase Price from the Purchaser, failing which GDN shall not be bound by the Licence with the Seller and GDN’s entry into a Licence with a Purchaser is subject to and conditional upon GDN entering into a related Licence with a Seller, failing which GDN shall not be bound by the Licence with the Purchaser.

3.13. A Licensed Asset may be downloaded by the Purchaser from the Marketplace immediately following notification of its availability for download by GDN.

3.14. Subject to clauses 3.8 and 3.16(d), during its availability on the Marketplace a Licensed Asset and each new version (if any) of that Licensed Asset may be downloaded by the Purchaser from the Marketplace no more than five times.

3.15. Should the Purchaser wish to obtain a full or partial refund in relation to a Licence, the Purchaser should contact GDN to mediate and resolve the dispute.

3.16. For the avoidance of doubt, neither the Seller nor GDN shall be obliged to provide a refund in respect of a Licence where the Purchaser;

a) no longer wishes to make use of a Licence or Licensed Asset;

b) purchased the Licence by mistake;

c) does not have sufficient expertise to use the Licensed Asset; or

d) can no longer access a Licensed Asset because it has been removed by its Seller from the Marketplace.

4.  LICENCE (A) – For purchases made after 00:00 (GMT) on 15th January 2019

4.1. A “Licence” means that the Seller grants to GDN (purely for the purpose of sub-licensing to the Purchaser) and GDN grants (by way of sub-licence thereof) to the Purchaser a non-exclusive perpetual licence to;

(a) use the Licensed Asset to create Derivative Works; and

(b) use the Licensed Asset and any Derivative Works as part of both Non-Monetized Media Products and Monetized Media Products, with no restriction on the number of projects the Licensed Asset may be used in. In either case, the Licensed Assets can be used in Media Products that are either:

i) used for the Purchaser’s own personal use; and/or

ii) used for the Purchaser’s commercial use in which case it may be distributed, sold and supplied by the Purchaser for any fee that the Purchaser may determine.

4.2. A Licence does not allow the Purchaser to:

(a) Use the Licensed Asset or Derivative Works in a logo, trademark or service mark;

(b) Use, sell, share, transfer, give away, sublicense or redistribute the Licensed Asset or Derivate Works other than as part of the relevant Media Product; or

(c) Allow the user of the Media Product to extract the Licensed Asset or Derivative Works and use them outside of the relevant Media Product.

5.  LICENCE (B) – For purchases made before 00:00 (GMT) on 15th January 2019

5.1. A “Licence” means that the Seller grants to GDN (purely for the purpose of sub-licensing to the Purchaser) and GDN grants (by way of sub-licence thereof) to the Purchaser a non-exclusive perpetual licence to;

(a) use the Licensed Asset to create Derivative Works; and

(b) use the Licensed Asset and any Derivative Works as part of either one (1) Non-Monetized Media Product or one (1) Monetized Media Product which, in either case, is:

i) used for the Purchaser’s own personal use; and/or

ii) used for the Purchaser’s commercial use in which case it may be distributed, sold and supplied by the Purchaser for any fee that the Purchaser may determine.

5.2. A Licence does not allow the Purchaser to:

(a) Use the Licensed Asset or Derivative Works in a logo, trademark or service mark;

(b) Use, sell, share, transfer, give away, sublicense or redistribute the Licensed Asset or Derivate Works other than as part of the relevant Non-Monetized Media Product or Monetized Media Product; or

(c) Allow the user of the Non-Monetized Media Product or Monetized Media Product to extract the Licensed Asset or Derivative Works and use them outside of the relevant Non-Monetized Media Product or Monetized Media Product.

5.3. In the case of a Licence in respect of multiple Licensed Assets in a bundle or pack, the Purchaser may use the Licensed Assets within the bundle or pack in respect of multiple Non-Monetized Media Products and/or Monetized Media Products provided that no one individual Licensed Asset within that bundle or pack is used more than once or in more than one Non-Monetized Media Product or Monetized Media Product.

5.4. A sequel to a Non-Monetized Media Products or Monetized Media Product is considered a separate Media Product in its own right and the use of any Licensed Assets howsoever in or in respect of such sequel requires and is conditional upon the purchase of a separate Licence in respect thereof.

6. SALE OF ASSETS AND PAYMENT PROVISIONS

6.1. By uploading any Assets to the Marketplace, the Seller acknowledges and accepts that 30% of the Purchase Price of each Licensed Asset received by GDN shall be retained by GDN by way of commission.

6.2. For each Licensed Asset, the Seller shall be entitled to payment by GDN of 70% of the Purchase Price received by GDN (“Seller’s Revenue”).

6.3. Subject to clause 6.4, on each occasion the total of the Seller’s Revenue arising from sale of Licensed Assets via the Marketplace reaches or exceeds the value of $50.00 (FIFTY US DOLLARS), the Seller may request payment of the Seller’s Revenue by GDN (“Withdrawal”).

6.4. The Seller may not make more than one Withdrawal in any two calendar weeks.

6.5. Any and all transaction fees due in connection with a Withdrawal (including, but not limited to, Paypal fees) shall be payable by the Seller. In the event that GDN is required to pay any such fees on behalf of the Seller, these shall be deducted by GDN from the Withdrawal payment made to the Seller.

6.6. VAT shall (if and to the extent applicable) be charged, accounted for and paid in respect of all transactions taking place via the Marketplace in accordance with all applicable laws and regulations and GDN’s lawful instructions and the Seller and Purchaser shall provide all such information, documentation and assistance and shall take such steps and actions as GDN may lawfully require for such purposes.

6.7. By uploading any Assets to the Marketplace, the Seller:

(a) agrees to grant the Licences to GDN for the purpose of sub-licensing the same to Purchasers;

(b) grants GDN a non-exclusive perpetual licence to:

(i) reproduce the Asset on the Marketplace;

(ii) make Licences in respect of the Asset available for sale to Purchasers via the Marketplace; and

(iii) use the Asset in relation to the advertising, promotion and distribution of our site and the Marketplace.

6.8. GDN may refuse, in its sole discretion, to allow Sellers to offer an Asset for license on the Marketplace. GDN is not required to give reasons for refusing to allow a Seller to offer an Asset for license on the Marketplace.

6.9. The Seller warrants that:

(a) it is the sole legal and exclusive owner of all intellectual property rights in each Asset;

(b) the Asset does not infringe the rights of any third party;

(c) the exercise by GDN of rights granted under these Marketplace Terms (including sub-licensing Licences to Purchasers) will not infringe the rights of any person;

(d) the Asset does not contain viruses or other computer codes, files or programs which are designed to limit or destroy the functionality of other computer software or hardware;

(e) the Asset shall not create liability for GDN or cause GDN to lose (in whole or in part) the services of its ISP or other suppliers;

(f) the Asset shall not cause GDN to violate any applicable law, statute, ordinance or regulation by making it available on the Marketplace; and

(g) the Asset complies with the Content Standards set out in our Acceptable Use Policy.

6.10. The Seller acknowledges and agrees that if GDN determines, in its absolute discretion, or is notified by a third party, that an Asset does not comply with any applicable laws and/or these Marketplace Terms, GDN may disable the Seller’s account and withhold all amounts that the Seller may have earned from the sale of Licences in respect of the relevant Asset until any dispute in relation to the Asset has been resolved to the satisfaction of GDN.

6.11. The Seller acknowledges and agrees that GDN may, at its sole discretion, elect at any time and for any reason to remove Assets from the Marketplace without notice to the Seller. GDN is not responsible for any loss that the Seller may suffer as a result an Asset being removed from the Marketplace by GDN.

7. OWNERSHIP OF ASSETS, DERIVATIVE WORKS AND MEDIA PRODUCT

7.1. Any and all intellectual property rights in the Asset shall be owned by the Seller.

7.2. Any and all intellectual property rights in Derivative Works shall be owned by the Seller. To the extent that any such intellectual property rights automatically vest in the Purchaser, then in consideration of the Licence granted to the Purchaser to create Derivative Works (which the Purchaser hereby acknowledges is adequate and sufficient consideration) the Purchaser hereby assigns by way of present and future assignment, any and all such rights to the Seller. The Purchaser shall execute and deliver such documents and perform such acts as may be required for the purpose of giving evidence of and/or full effect to such assignment.

7.3. Subject to clauses 7.1 and 7.2, any and all intellectual property rights in the Media Product shall be owned by the Purchaser.

8. LIMITATIONS AND EXCLUSIONS OF LIABILITY

8.1. This clause 8 applies in addition to the limitations and exclusions of GDN’s liability set out in the Terms of Website Use.

8.2. GDN does not have any control over, and does not take any responsibility for, the quality, safety or legality of any Asset uploaded to, or downloaded from, the Marketplace by a Member.

8.3. GDN does not warrant that the Assets or any content, code, data or materials uploaded to, or downloaded from, the Marketplace does not infringe the intellectual property rights of a third party. Each Seller is required to warrant that its Asset does not infringe the intellectual property rights of any third party.

8.4. To the fullest extent permitted by law, GDN shall not be liable to any Member for any costs, expenses, loss or damage (whether direct, indirect or consequential and whether economic or other) arising from GDN’s exercise of the rights granted to it under these Marketplace Terms (including sub-licensing Licences to Purchasers).

8.5. Each Member shall indemnify GDN against all liabilities, costs, expenses, damages or losses (including any direct, indirect or consequential losses, loss of profit, loss of reputation and all interest, penalties and legal costs (calculated on a full indemnity basis) and all other professional costs and expenses) suffered or incurred by GDN arising out of or in connection with:

(a) GDN’s exercise of the rights granted to it under these Marketplace Terms (including sub-licensing Licences to Purchasers); and/or

(b) the enforcement of these Marketplace Terms.

8.6. Nothing in these Marketplace Terms shall have the effect of excluding or limiting any liability for death or personal injury caused by negligence or any liability for fraud or fraudulent misrepresentation.

9. GENERAL

9.1. GDN may sub-contract, sub-license, delegate or otherwise transfer GDN’s rights and obligations under these Marketplace Terms to another organisation, but this will not affect a Member’s rights under these Marketplace Terms.

9.2. A Member may only sub-contract, sub-license, delegate or otherwise transfer its rights or its obligations under these Marketplace Terms to another person if GDN agrees in writing.

9.3. Each clause of these Marketplace Terms operates separately. If any court or relevant authority decides that any of them are unlawful or unenforceable, the remaining clauses will remain in full force and effect.

9.4. If GDN fails to insist that a Member performs any of its obligations under these Marketplace Terms, or if GDN does not enforce GDN’s rights against a Member, or if GDN delays in doing so, that will not mean that GDN has waived GDN’s rights against the Member and will not mean that Members do not have to comply with those obligations. If GDN does waive a default by a Member, GDN will only do so in writing, and that will not mean that GDN will automatically waive any later default by a Member.

9.5. These Marketplace Terms are governed by English law. This means these Marketplace Terms and any dispute or claim arising out of or in connection with them or the Marketplace will be governed by English law. The courts of England and Wales will have exclusive jurisdiction.
