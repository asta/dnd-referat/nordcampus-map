[![pipeline status](https://gitlab.gwdg.de/wa/kif490-workadventure-map/badges/master/pipeline.svg)](https://gitlab.gwdg.de/wa/kif490-workadventure-map/-/commits/master)

# KIF49 Map

This repositor is intended to contain a map for the KIF49 Workadventure!
Visit the Adventure [here](https://world.kif.rocks)

## Test-Server

Einen Server der die Karte lädt findet sich [hier](https://world.oh14.de/_/global/wa.pages.gwdg.de/kif490-workadventure-map/). 

## TO-DO:

### must have:
- [x] Orga Office
- [x] Timetable Link
- [x] Awareness-Team Office
- [ ] Link to livestream (ommit if possible)

### nice to have:
- [x] "Ewiges Frühstück"
- [x] Gametables
  - [x] Gartic Phone
  - [x] Settlers of Catan Clon
  - [ ] Agar.io
  - [x] Scribble.io
- [ ] KIF-Café
